<? 

require_once 'lib/Fz/UserAgent.php';
require_once 'lib/Fz/utils/get_base_href.php';

$base_href = get_base_href();

$ua = new Fz_UserAgent();

if ( $ua->is_mobile() ) 
{
	$url = strpos( $_SERVER[ 'HTTP_HOST' ], 'dev.' ) !== false ? 'http://m.dev.breaknazirsman.com' : 'http://m.breaknazirsman.com';
	header( 'Location: ' . $url );
}

$is_tablet = $ua->is_tablet();
$is_ipad = $ua->is_ipad();
$doc_class = $is_tablet ? 'tablet' : 'desktop';

if ( $is_ipad )
{
	$doc_class .= ' ipad';
}

/* EOF */