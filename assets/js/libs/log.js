define(function() {
	'use strict';

    var consoleLogIsAvailable = ( typeof window.console != 'undefined' && typeof window.console.log != 'undefined' );
	
    // conditional for IE
    return window.log = consoleLogIsAvailable && typeof console.log.apply === 'undefined' ? console.log : function() {
		if ( consoleLogIsAvailable ) {
			if ( typeof console.log.apply != 'undefined' ) {
				console.log.apply( console, arguments );
			} else {
				window.log = console.log;
			}
		}
	};
});
