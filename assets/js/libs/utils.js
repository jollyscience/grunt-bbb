define(function() {
	'use strict';

	// template utils
	return window.utils = window.utils || {
	    escapeHtml: function( str ) {
	        return str.replace( /&/g,'&amp;' )
	            .replace( />/g,'&gt;' )
	            .replace( /</g,'&lt;' )
	            .replace( /"/g,'&quot;' )
	            .replace( /'/g,'&#39;' );
	    },

	    limit: function( str, limit, ellipsis ) {
		ellipsis = ellipsis || '...';

		if ( !str ) {
			return;
		}
		
		return str.length > limit ? str.substr( 0, limit ) + ellipsis : str;
	    },

	    removeProtocol: function( str ) {
	        return str.replace( 'http:', '' )
			.replace( 'https:', '' );
	    }
	};
});
