// `app/config.js`
// # Configuration
// Set the require.js configuration for your application.
require.config({
    // Initialize the application with the main application file.
    deps: ["../assets/js/libs/log", "main", "tracking"],
    paths: {
        // ## JavaScript folders.
        libs: "../assets/js/libs",
        plugins: "../assets/js/plugins",
        // ## Libraries.
        jquery: "../assets/js/libs/jquery",
        lodash: "../assets/js/libs/lodash",
        backbone: "../assets/js/libs/backbone.amd",
        // ## Plugins
        text: "../assets/js/plugins/text",
        json: "../assets/js/plugins/json",
        // ### LayoutManager
        // NOTE: We are using 0.6.6 here, which is not 
        // packaged with grunt-bbb at the time of writing.
        // This upgrade is preferred for it's simpler handling
        // of view renders, with beforeRender and afterRender
        layoutmanager: "../assets/js/plugins/backbone.layoutmanager.0.6.6",
        // ### LocalStorage
        // BUG: This doesn't work yet.
        localstorage: "../assets/js/plugins/backbone-localstorage"
    },
    shim: {
        // Backbone library depends on lodash and jQuery.
        backbone: {
            deps: ["lodash", "jquery"],
            exports: "Backbone"
        },
        // Backbone.LayoutManager depends on Backbone.
        'layoutmanager': {
            deps: ['backbone'],
            exports: 'Backbone.LayoutManager'
        },
        // Backbone.LocalStorage depends on Backbone.
        'localstorage': {
            deps: ['backbone', 'lodash'],
            exports: 'Backbone.LocalStorage'
        }
    }
});