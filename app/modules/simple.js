// `app/modules/simple.js`
// # Simple Module Example
// This example module exists to show how a module
// and layout can co-exist with minimal input, 
// and lots of automatic controls from within bbb.
define(["jquery", "lodash", "backbone", "app"],

function ($, _, Backbone, app) {

    // ## Views
    // In a more complex module, you'll likely want
    // to setup a 'package' of module components,
    // alla the Todo Module. However, for this
    // example, we're just including a single object 
    // of each type (MV*), so we're keeping it all in one file.
    var Views = {};

    Views.Main = Backbone.View.extend({
        // Per Backbone.LayoutManager, this will auto-load
        // the template, serialize a model if it exists,
        // and render accordingly. All you really need here
        // is the template. So that's all we have.
        // This translates into 'app/templates/simple.html'.
        template: "simple"
    });

    // ## Create a new module.
    var Simple = app.module({
        // ### setViews
        // As a simple example of how to load a view into a 
        // selector within the current layout, we're going to 
        // dump the contents of app/templates/simple.html
        // into the #simple_example div that exists in 
        // app/templates/layout/simple.html.
        setViews: function () {
            return {
                "#simple_example": new Views.Main()
            };
        }
    });

    // Return the module for AMD compliance.
    return Simple;

});