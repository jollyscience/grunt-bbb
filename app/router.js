// `app/router.js`
// # Main Router
define([
    /* Include all the major libs. */
    "jquery", "lodash", "backbone", "app",
    /* include all of the site's primary modules (or 'pages'). */
    "modules/todo", "modules/simple"], function ($, _, Backbone, app, Todo, Simple) {

    var Router = Backbone.Router.extend({

        // Match all of the desired routes to
        // the appropriate functions ("route": "function").
        routes: {
            // A simple example.
            "simple": "simple",
            // A complex example.
            "todo": "todo",
            // Default example.
            "hello": "hello",
            // If no route is passed.
            "": "hello",
            // If a failing route is passed.
            "*notFound": "hello"
        },

        // ### Static page example.
        hello: function () {
            // This will render a static
            // layout, without trying to
            // pass it any data, and without
            // instantiating any modules.
            this.updatePage({
                layout: "hello"
            });
        },

        // ### Simple page example with no subViews.
        simple: function () {
            // This will load a static layout,
            // and allow the assigned module 
            // ('app/modules/simple.js')
            // to handle any customized functionality.
            this.updatePage({
                layout: "simple",
                module: Simple
            });
        },

        // ### Complex page example with subViews.
        todo: function () {
            // Same as 'Simple' above, except for the
            // complexity of the Todo Module. 
            // (see: 'app/modules/todo.js' for details)
            this.updatePage({
                module: Todo,
                layout: "todo"
            });
        },

        // ## Custom Routing Methods.
        // ### UpdatePage is the main function.
        // @param: Object
        updatePage: function (o) {
            // Since the updatePage method needs flexibility,
            // we can handle a currentModule of null, and 
            // just render a static layout.
            app.currentModule = (o.module) ? o.module : null;
            // If no layout is passed, we can publish 
            // the default Layout from app.
            app.currentLayout = (o.layout) ? o.layout : app.defaultLayout;
            // Finally, we trigger a logical string of
            // methods, mirroring the methods that will 
            // be called on the currentModule, if they
            // exist.
            this.beforeRender().then(this.render).then(this.afterRender);
        },

        // ### beforeRender
        beforeRender: function () {
            // First, we create a Deferred object, to help us
            // manage the load sequence properly. 
            // (This is in case a module takes a while to load,
            // and / or has dependencies to wait for itself).
            var d = $.Deferred();
            // Only try to run beforeRender on the Module
            // if it actually has such a function.
            if (app.currentModule !== null && _.isFunction(app.currentModule.beforeRender)) {
                $.when(app.currentModule.beforeRender()).done(d.resolve);
            } else {
                d.resolve();
            }
            return d.promise();
        },

        // ### render
        render: function () {
            var d;
            // Only try to run setViews if the Module
            // has such a function.
            if (app.currentModule !== null && _.isFunction(app.currentModule.setViews)) {
                d = app.useLayout(app.currentLayout).setViews(app.currentModule.setViews()).render();
            } else {
                // Either way, we need to tell app to
                // useLayout, and render the current Layout.
                d = app.useLayout(app.currentLayout).render();
            }
            return d;
        },

        // ### afterRender
        afterRender: function () {
            // Only try to run afterRender if the Module
            // actually has such a function.
            if (app.currentModule !== null && _.isFunction(app.currentModule.afterRender)) {
                return app.currentModule.afterRender(app.currentModule);
            }
        },

        // ## Utility
        getRoutes: function () {
            return _.chain(this.routes).values().union().value();
        }

    });

    return Router;

});