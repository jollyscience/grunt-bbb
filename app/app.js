// app/app.js
define([
    "jquery",
    "lodash",
    "backbone",
    /* Include the default nav */
    'text!templates/nav.html',
    "json!../package.json",
    "layoutmanager"
    ],

function ($, _, Backbone, navHtml, pkg) {
    log(pkg.description + ' : ' + pkg.version);

    // Provide a global location to place configuration settings and module
    // creation.
    var app = {
        // The root path to run the application.
        root: "/",
        // Set the default layout for when 
        // routes don't designate a layout.
        defaultLayout: "main",
        // Set the main container div.
        container: "#main",
        // Create a nav object to allow for 
        // quickly moving between available routes.
        nav: {
            container: "#nav",
            display: true,
            docs: true
        },
        // Import the package file, so the app
        // can report on things like version bumps.
        pkg: pkg
    };

    // Localize or create a new JavaScript Template object.
    var JST = window.JST = window.JST || {};

    // Configure LayoutManager with Backbone Boilerplate defaults.
    Backbone.LayoutManager.configure({
        manage: true,
        paths: {
            layout: "app/templates/layouts/",
            template: "app/templates/"
        },

        fetch: function (path) {
            path = path + ".html";

            if (!JST[path]) {
                $.ajax({
                    url: app.root + path,
                    async: false
                }).then(function (contents) {
                    JST[path] = _.template(contents);
                });
            }

            return JST[path];
        }
    });

    // Mix Backbone.Events, modules, and layout management into the app object.
    return _.extend(app, {
        // Create a custom object with a nested Views object.
        module: function (additionalProps) {
            return _.extend({
                Views: {}
            }, additionalProps);
        },

        updateNav: function () {
            var tmp = _.template(navHtml),
                routes = app.router.getRoutes(),
                items = [];

            _.each(routes, function (route) {
                items.push({
                    title: route,
                    url: route
                });
            }, this);
            
            if (app.nav.docs) {
	            items.push({
		            title: 'docs',
		            url: 'docs/app.html'
	            });
            }

            $(app.nav.container).html(tmp({
                items: items,
                project: {
                    name: app.pkg.name,
                    description: app.pkg.description,
                    version: app.pkg.version
                }
            }));
        },

        // Helper for using layouts.
        useLayout: function (name) {
            if (this.nav.display) {
                this.updateNav();
            }

            // If already using this Layout, then don't re-inject into the DOM.
            if (this.layout && this.layout.options.template === name) {
                return this.layout;
            }

            // If a layout already exists, remove it from the DOM.
            if (this.layout) {
                this.layout.remove();
            }

            // Create a new Layout.
            var layout = new Backbone.Layout({
                template: name,
                className: "layout " + name,
                id: "layout"
            });

            // Insert into the DOM.
            $(app.container).empty().append(layout.el);

            // Render the layout.
            layout.render();

            // Cache the refererence.
            this.layout = layout;

            // Return the reference, for chainability.
            return layout;
        }
    }, Backbone.Events);

});