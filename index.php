<? include 'inc/init.php'; ?>
<!doctype html>
<html lang="en" class="<?= $doc_class ?>">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
	<meta name="page-tracking" content="series:homeland:extras:it hits home:pillars:interrogation">
	<base href="<?= $base_href ?>">
	<title>SITE TITLE</title>

	<link href='http://fonts.googleapis.com/css?family=Cantarell:400,700,700italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="assets/css/index.css">
	
	<!--[if lt IE 9]>
	<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->
</head>
<body class="loading">
	<!-- scroll target -->
	<span id="top" style="display:block; position:absolute; top:0;">&nbsp;</span>
	
	<div id="nav"></div>
	<!-- Main container -->
	<div role="main" id="main" class="clearfix container-fluid"></div>

	<!-- Application source -->
	<script type="text/javascript" data-main="app/config" src="assets/js/libs/require.js"></script>

</body>
</html>